<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveCptCntQuote',
            'Hivequotelistquote',
            [
                'Quote' => 'list'
            ],
            // non-cacheable actions
            [
                'Quote' => 'list'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveCptCntQuote',
            'Hivequoteshowquote',
            [
                'Quote' => 'list, show'
            ],
            // non-cacheable actions
            [
                'Quote' => 'list, show'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hivequotelistquote {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_cpt_cnt_quote') . 'Resources/Public/Icons/user_plugin_hivequotelistquote.svg
                        title = LLL:EXT:hive_cpt_cnt_quote/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_quote_domain_model_hivequotelistquote
                        description = LLL:EXT:hive_cpt_cnt_quote/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_quote_domain_model_hivequotelistquote.description
                        tt_content_defValues {
                            CType = list
                            list_type = hivecptcntquote_hivequotelistquote
                        }
                    }
                    hivequoteshowquote {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_cpt_cnt_quote') . 'Resources/Public/Icons/user_plugin_hivequoteshowquote.svg
                        title = LLL:EXT:hive_cpt_cnt_quote/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_quote_domain_model_hivequoteshowquote
                        description = LLL:EXT:hive_cpt_cnt_quote/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_quote_domain_model_hivequoteshowquote.description
                        tt_content_defValues {
                            CType = list
                            list_type = hivecptcntquote_hivequoteshowquote
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
