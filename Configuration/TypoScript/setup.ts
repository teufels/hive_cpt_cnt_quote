
plugin.tx_hivecptcntquote_hivequotelistquote {
    view {
        templateRootPaths.0 = EXT:hive_cpt_cnt_quote/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hivecptcntquote_hivequotelistquote.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_cpt_cnt_quote/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hivecptcntquote_hivequotelistquote.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_cpt_cnt_quote/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hivecptcntquote_hivequotelistquote.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hivecptcntquote_hivequotelistquote.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

plugin.tx_hivecptcntquote_hivequoteshowquote {
    view {
        templateRootPaths.0 = EXT:hive_cpt_cnt_quote/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hivecptcntquote_hivequoteshowquote.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_cpt_cnt_quote/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hivecptcntquote_hivequoteshowquote.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_cpt_cnt_quote/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hivecptcntquote_hivequoteshowquote.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hivecptcntquote_hivequoteshowquote.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

# these classes are only used in auto-generated templates
plugin.tx_hivecptcntquote._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-hive-cpt-cnt-quote table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-hive-cpt-cnt-quote table th {
        font-weight:bold;
    }

    .tx-hive-cpt-cnt-quote table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)




## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin {
    tx_hivecptcntquote{

        # Standard PID aus den Konstanten
        persistence {
            storagePid = {$plugin.tx_hivecptcntquote.persistence.storagePid}
        }

        model {
            HIVE\HiveCptCntQuote\Domain\Model\Quote {
                persistence {
                    storagePid = {$plugin.tx_hivecptcntquote.model.HIVE\HiveCptCntQuote\Domain\Model\Quote.persistence.storagePid}
                }
            }
        }

        classes {
            HIVE\HiveCptCntQuote\Domain\Model\Quote.newRecordStoragePid = {$plugin.tx_hiveextcontact.model.HIVE\HiveExtCountry\Domain\Model\Quote.persistence.storagePid}
            HIVE\HiveCptCntQuote\Domain\Model\Quote {
                mapping {
                    recordType = 0
                    tableName = sys_category
                }
            }
        }

    }
}