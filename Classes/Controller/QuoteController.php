<?php
namespace HIVE\HiveCptCntQuote\Controller;

/***
 *
 * This file is part of the "hive_cpt_cnt_quote" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *           Yannick Aister <y.aister@teufels.com>, teufels GmbH
 *
 ***/

/**
 * QuoteController
 */
class QuoteController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * quoteRepository
     *
     * @var \HIVE\HiveCptCntQuote\Domain\Repository\QuoteRepository
     * @inject
     */
    protected $quoteRepository = null;


    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        // get settings
        $aSettings = $this->settings;
        if (array_key_exists('bDebug', $aSettings) && array_key_exists('sDebugIp', $aSettings)) {
            $aDebugIp = explode(',', $aSettings['sDebugIp']);
            if ($aSettings['bDebug'] == '1' && (in_array($_SERVER['REMOTE_ADDR'], $aDebugIp) or $aSettings['sDebugIp'] == '*')) {
                \TYPO3\CMS\Core\Utility\DebugUtility::debug($aSettings, 'Debug: ' . __FILE__ . ' in Line: ' . __LINE__);
            }
        }

        // get plugin uid
        $iPluginUid = $this->configurationManager->getContentObject()->data['uid'];

        // get selected quotes if not empty
        $oQuotes = $this->quoteRepository->findByUidListOrderByListIfNotEmpty($aSettings['oHiveCptCntQuote']['main']['mn']);

        // get all quotes if no selection
        if($oQuotes === NULL) {
            $oQuotes = $this->quoteRepository->findAll();
        }

        // slice quotes if limit is set
        $iMaxItems = (int)$aSettings[maxItems];
        if ($iMaxItems != NULL) {
            $oQuotes = array_slice($oQuotes, 0, $iMaxItems);
        }

        $this->view->assign('oQuotes', $oQuotes);
        $this->view->assign('aSettings', $aSettings);
        $this->view->assign('iPluginUid', $iPluginUid);
    }

    /**
     * action show
     *
     * @param \HIVE\HiveCptCntQuote\Domain\Model\Quote $quote
     * @return void
     */
    public function showAction(\HIVE\HiveCptCntQuote\Domain\Model\Quote $quote)
    {
        // get metadata
        $oMetaData = $quote->getMetaData();
        if ($oMetaData != '') {
            $sMetaObjectAlternativeTitle = $oMetaData->getAlternativeTitle();
            $sMetaObjectKeywords = $oMetaData->getKeywords();
            $sMetaObjectDescription = $oMetaData->getDescription();
        }
        // metaTITLE
        if ($sMetaObjectAlternativeTitle != '') {
            $sMetaTitleContent = strip_tags($sMetaObjectAlternativeTitle);
        } else {
            $sMetaTitleContent = strip_tags($quote->getTitle());
        }
        $sOgMetaTitle = '<meta name="og:title" content="' . $sMetaTitleContent . '">';
        $sDCMetaTitle = '<meta name="DC.title" content="' . $sMetaTitleContent . '">';
        $GLOBALS['TSFE']->page['title'] = $sMetaTitleContent;
        $this->response->addAdditionalHeaderData($sOgMetaTitle . PHP_EOL . $sDCMetaTitle);
        // metaKEYWORDS
        if ($sMetaObjectKeywords != '') {
            $sMetaKeywordsContent = strip_tags($sMetaObjectKeywords);
            $sMetaKeywords = '<meta name="keywords" content="' . $sMetaKeywordsContent . '">';
            $sOgMetaKeywords = '<meta name="og:keywords" content="' . $sMetaKeywordsContent . '">';
            $sDCMetaKeywords = '<meta name="DC.keywords" content="' . $sMetaKeywordsContent . '">';
            $this->response->addAdditionalHeaderData($sMetaKeywords . PHP_EOL . $sOgMetaKeywords . PHP_EOL . $sDCMetaKeywords);
        }
        // metaDESCRIPTION
        if ($sMetaObjectDescription != '') {
            $sMetaDescriptionContent = strip_tags($sMetaObjectDescription);
//        } else {
//            $sMetaDescriptionContent = strip_tags($quote->getLongDescription());
//        }
            $sMetaDescription = '<meta name="description" content="' . $sMetaDescriptionContent . '">';
            $sOgMetaDescription = '<meta name="og:description" content="' . $sMetaDescriptionContent . '">';
            $sDCMetaDescription = '<meta name="DC.description" content="' . $sMetaDescriptionContent . '">';
            $this->response->addAdditionalHeaderData($sMetaDescription . PHP_EOL . $sOgMetaDescription . PHP_EOL . $sDCMetaDescription);
        }

        $this->view->assign('quote', $quote);
    }
}
